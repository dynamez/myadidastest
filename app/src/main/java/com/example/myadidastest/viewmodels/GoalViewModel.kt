package com.example.myadidastest.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.myadidastest.database.GoalDatabase
import com.example.myadidastest.models.Goal
import com.example.myadidastest.models.GoalResponse
import com.example.myadidastest.network.ApiFactory
import com.example.myadidastest.repositories.GoalRepository
import kotlinx.coroutines.*
import org.jetbrains.anko.doAsync
import kotlin.coroutines.CoroutineContext

class GoalViewModel(application: Application) : AndroidViewModel(application) {

    companion object{
        const val TAG = "GoalViewModel"
    }
    private var mDb: GoalDatabase? = GoalDatabase.getInstance(application)


    private val parentJob = Job()
    private val coroutineContext : CoroutineContext
    get() = parentJob + Dispatchers.Main

    private val scope = CoroutineScope(coroutineContext)

    private val repository: GoalRepository = GoalRepository(ApiFactory.goalApi)

    val goalLiveData = MutableLiveData<GoalResponse>()
    internal val dataBaseLiveData: LiveData<List<Goal>> = mDb?.goalDao()!!.allGoals()
    val currentProgress = MutableLiveData<Int>()

    fun getGoals(){
        scope.launch {
            val response = repository.getGoals()
            goalLiveData.postValue(response)
        }
    }

    fun insertGoals(goals: GoalResponse){
        goals.items.let {
            for (goal in it){
                doAsync {
                    mDb?.goalDao()?.insertGoals(goal)
                }
            }
        }

    }

    fun cancelallRequest() = coroutineContext.cancel()

    fun deleteAll(){
        mDb?.goalDao()?.deleteAll()
    }
    fun insert(goal: Goal){
        mDb?.goalDao()?.insertGoals(goal)
    }
}