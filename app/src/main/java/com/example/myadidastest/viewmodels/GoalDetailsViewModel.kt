package com.example.myadidastest.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.myadidastest.models.Goal


class GoalDetailsViewModel : ViewModel(){

    var goal: MutableLiveData<Goal> = MutableLiveData()
    var progress: MutableLiveData<Int> = MutableLiveData()


}