package com.example.myadidastest.views

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myadidastest.R
import com.example.myadidastest.adapter.GoalAdapter
import com.example.myadidastest.viewmodels.GoalViewModel
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.fitness.Fitness
import com.google.android.gms.fitness.FitnessOptions
import com.google.android.gms.fitness.data.DataType
import com.google.android.gms.fitness.data.Field
import com.google.android.gms.tasks.OnFailureListener
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
private lateinit var viewModel: GoalViewModel

    companion object{
        const val TAG = "MainActivity"
        const val GOOGLE_FIT_PERMISSIONS_REQUEST_CODE = 1
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        viewModel = ViewModelProviders.of(this).get(GoalViewModel::class.java)

        viewModel.dataBaseLiveData.observe(this@MainActivity, Observer {
            Log.i("ROOM", it.toString())
        })


        viewModel.getGoals()
        viewModel.goalLiveData.observe(this, Observer {
            if (it != null){
                if (it.items.isNotEmpty()){
                    loading_icon.visibility = View.INVISIBLE
                    loading_text.visibility = View.INVISIBLE
                }

                viewModel.insertGoals(it)

            }
        })
        val fitnessOptions = FitnessOptions.builder()
                .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.AGGREGATE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                .build()

        if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(this), fitnessOptions)) {
            GoogleSignIn.requestPermissions(
                    this, // your activity
                    GOOGLE_FIT_PERMISSIONS_REQUEST_CODE,
                    GoogleSignIn.getLastSignedInAccount(this),
                    fitnessOptions)
        } else {
            readData()
        }


//
    }

    private fun readData() {

        Fitness.getHistoryClient(this, GoogleSignIn.getLastSignedInAccount(this)!!)
                .readDailyTotal(DataType.TYPE_STEP_COUNT_DELTA)
                .addOnSuccessListener { dataSet ->
                    val total = (if (dataSet.isEmpty)
                        0
                    else
                        dataSet.dataPoints[0].getValue(Field.FIELD_STEPS).asInt()).toLong()
                    viewModel.currentProgress.value = total.toInt()
                    recycler_goals.apply {
                        setHasFixedSize(true)
                        layoutManager = LinearLayoutManager(this@MainActivity)
                        adapter = GoalAdapter(
                                viewModel,
                                this@MainActivity,
                                ItemClickListener()
                        )
                    }
                    Log.i(TAG, "Total steps: $total")
                }
                .addOnFailureListener(
                        object: OnFailureListener {
                            override fun onFailure(e:Exception) {
                                Log.w(TAG, "There was a problem getting the step count.", e)
                            }
                        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == GOOGLE_FIT_PERMISSIONS_REQUEST_CODE){
            if (resultCode == -1){
                readData()
            }
            Log.i("RESULT CODE", resultCode.toString())
            Log.i("DATA", data.toString())
        }
    }
    private inner class ItemClickListener : GoalAdapter.ItemClickListener<GoalAdapter.ViewHolder> {
        override fun onItemClick(holder: GoalAdapter.ViewHolder) {

            val intent = Intent(this@MainActivity, GoalDetails::class.java)

            intent.putExtra("goalData", holder.binding.item)
            intent.putExtra("progress", viewModel.currentProgress.value)

            this@MainActivity.startActivity(intent)
            Log.d(TAG, "ITEM CLICKED!")

        }
    }

}
