package com.example.myadidastest.views

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.myadidastest.R
import com.example.myadidastest.models.Goal
import com.example.myadidastest.viewmodels.GoalDetailsViewModel
import kotlinx.android.synthetic.main.activity_goal_details.*

class GoalDetails : AppCompatActivity() {

    private lateinit var viewModel: GoalDetailsViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_goal_details)
        val bundle: Bundle? = intent.extras
        viewModel = ViewModelProviders.of(this).get(GoalDetailsViewModel::class.java)
        bundle?.let {
            bundle.apply {
                //Parcelable Data
                val goal: Goal? = getParcelable("goalData")
                var progress: Int? = getInt("progress")

                if (goal != null) {
                    viewModel.goal.value = goal
                    viewModel.progress.value = progress
//                    viewModel.description.value = goal.description
                }
            }
        }


        viewModel.goal.observe(this, Observer {
            detail_title.text = it.title
            details_description.text = it.description
            type_value.text = when(it.type){
                "step" -> "Steps"
                "running_distance" -> "Running"
                "walking_distance" -> "Walking"
                else -> "Invalid Type"
            }
            goal_value.text = "${it.goal} Steps"
            points_value.text = it.reward.points.toString()
            trophy_value.text = when(it.reward.trophy){
                "silver_medal" -> "Silver Medal"
                "bronze_medal" -> "Bronze Medal"
                "gold_medal" -> "Gold Medal"
                "zombie_hand" -> "Zombie Hand"
                else -> "No Medal"
            }
            animation.setAnimation(when(it.reward.trophy){
                "silver_medal" -> "silver.json"
                "bronze_medal" -> "bronze.json"
                "gold_medal" -> "trophy.json"
                "zombie_hand" -> "trophy.json"
                else -> "trophy.json"
            })
            animation.playAnimation()



        })

        viewModel.progress.observe(this, Observer {
            progress_value.text = it.toString()
        })





    }
}
