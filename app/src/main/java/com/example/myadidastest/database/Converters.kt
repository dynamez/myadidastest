package com.example.myadidastest.database

import androidx.room.TypeConverter
import com.example.myadidastest.models.Reward
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


class Converters {

    @TypeConverter
    fun fromString(string: String) : Reward {

        val listType = object : TypeToken<Reward>() {

        }.type
        return Gson().fromJson(string, listType)
    }

    @TypeConverter
    fun fromReward(reward: Reward): String {
        val gson = Gson()
        return gson.toJson(reward)

    }
}