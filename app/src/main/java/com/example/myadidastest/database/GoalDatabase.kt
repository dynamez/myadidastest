package com.example.myadidastest.database

import android.content.Context

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.myadidastest.dao.GoalDao
import com.example.myadidastest.models.Goal

@Database(entities = arrayOf(Goal::class), version = 1)
@TypeConverters(Converters::class)
abstract class GoalDatabase : RoomDatabase() {

    abstract fun goalDao(): GoalDao

    companion object {
        private var INSTANCE: GoalDatabase? = null

        fun getInstance(context: Context): GoalDatabase? {
            if (INSTANCE == null) {
                synchronized(GoalDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                            GoalDatabase::class.java, "goals.db")
                            .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}