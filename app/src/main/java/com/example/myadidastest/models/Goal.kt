package com.example.myadidastest.models

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "goals")
@Parcelize
data class Goal (
        @PrimaryKey
        val id: String,
        val title: String,
        val description: String,
        val type: String,
        val goal: Int,
        val reward: Reward
): Parcelable