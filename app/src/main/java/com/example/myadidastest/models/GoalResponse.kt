package com.example.myadidastest.models

data class GoalResponse (
        val items: List<Goal>,
        val nextPageToken: String
)