package com.example.myadidastest.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.myadidastest.models.Goal

@Dao
interface GoalDao{
    @Query("SELECT * FROM goals")
    fun allGoals(): LiveData<List<Goal>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertGoals(goal: Goal)

    @Query("delete from goals")
    fun deleteAll()


}