package com.example.myadidastest.services

import com.example.myadidastest.models.GoalResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET

interface GoalApi {

    @GET("goals")
    fun getGoals() : Deferred<Response<GoalResponse>>
}