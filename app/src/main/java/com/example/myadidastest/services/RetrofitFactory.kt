package com.example.myadidastest.services

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object RetrofitFactory {

    private val networkClient = OkHttpClient().newBuilder()
            .build()

    fun retrofit() : Retrofit = Retrofit.Builder()
            .client(networkClient)
            .baseUrl("https://thebigachallenge.appspot.com/_ah/api/myApi/v1/")
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
}