package com.example.myadidastest.repositories

import android.util.Log
import com.example.myadidastest.network.GoalsResult
import retrofit2.Response
import java.io.IOException

open class BaseRepository {

    suspend fun <T : Any> safeApiCall(call: suspend () -> Response<T>, errorMessage: String): T? {

        val result: GoalsResult<T> = safeApiResult(call, errorMessage)
        var data: T? = null

        when(result){
            is GoalsResult.Success ->
                data = result.data
            is GoalsResult.Error -> {
                Log.d("BaseRepository", "$errorMessage % Exception - ${result.exception}")
            }
        }
        return data
    }

    private suspend fun <T: Any> safeApiResult(call: suspend () -> Response<T>, errorMessage: String): GoalsResult<T> {
        val response = call.invoke()
        if (response.isSuccessful) return GoalsResult.Success(response.body()!!)

        return GoalsResult.Error(IOException("Error ocurred while getting safe api results, Custom Error - $errorMessage"))
    }
}