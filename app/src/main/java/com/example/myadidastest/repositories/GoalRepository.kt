package com.example.myadidastest.repositories

import com.example.myadidastest.services.GoalApi
import kotlinx.coroutines.runBlocking

class GoalRepository(
    private val api: GoalApi
) : BaseRepository(){

    fun getGoals() = runBlocking {
        safeApiCall(
                call = {api.getGoals().await()},
                errorMessage = "Error gettin goals"
        )
    }



}