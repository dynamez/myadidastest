package com.example.myadidastest.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.example.myadidastest.databinding.GoalItemBinding
import com.example.myadidastest.models.Goal
import com.example.myadidastest.viewmodels.GoalViewModel

class GoalAdapter (
        private val goalViewModel: GoalViewModel,

        private val lifecycleOwner: LifecycleOwner,
        private val itemClickListener: ItemClickListener<ViewHolder>
): RecyclerView.Adapter<GoalAdapter.ViewHolder>() {
private var goalList : List<Goal> = emptyList()

init {
    goalViewModel.dataBaseLiveData.observe(lifecycleOwner, Observer {
        Log.d("Adapter!", it.toString())
        updateUI(it)
    })
}

    fun updateUI(goalList: List<Goal>){
        this.goalList = goalList
        notifyDataSetChanged()
    }
    override fun getItemCount(): Int {
        return goalList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {


        val inflater = LayoutInflater.from(parent.context)
        val binding = GoalItemBinding.inflate(inflater, parent, false)
        val holder = ViewHolder(binding)

        holder.itemClickListener = itemClickListener
        return holder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.binding.item = goalList[position]
    }

    inner class ViewHolder(val binding: GoalItemBinding) : RecyclerView.ViewHolder(binding.root), View.OnClickListener{


        init {
            binding.root.setOnClickListener(this)
        }

        var goal: Goal? = null
        var itemClickListener: ItemClickListener<ViewHolder>? = null
        override fun onClick(v: View?) {

            itemClickListener?.onItemClick(this)
        }
    }
    interface ItemClickListener<StockViewHolder> {
        fun onItemClick(holder: StockViewHolder)
    }

}