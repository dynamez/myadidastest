package com.example.myadidastest.network

import com.example.myadidastest.services.GoalApi
import com.example.myadidastest.services.RetrofitFactory

object ApiFactory {


    val goalApi : GoalApi = RetrofitFactory.retrofit().create(GoalApi::class.java)
}