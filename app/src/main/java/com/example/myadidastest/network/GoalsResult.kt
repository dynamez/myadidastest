package com.example.myadidastest.network

import java.lang.Exception

sealed class GoalsResult <out T: Any> {

    data class Success<out T : Any>(val data :T) : GoalsResult<T>()
    data class Error(val exception: Exception) : GoalsResult<Nothing>()
}